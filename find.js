
function find(elements,cb)
{
    let fo=undefined;

    for(let i=0;i<elements.length;i++)
    {
        if(cb(elements[i]))
        {
        fo=elements[i];
        return fo;
        }
    }

    return fo;

}

module.exports=find;