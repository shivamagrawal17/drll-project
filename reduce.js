
function reduce(elements,cb,startingvalue)
{
    i=startingvalue || 0;
    let combined=elements[i];
    for(let j=i+1;j<elements.length;j++)
    {
        combined=cb(combined,elements[j]);
    }
    return combined;
}

module.exports=reduce;